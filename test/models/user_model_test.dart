
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/user.dart';

import '../resource/fake_user.dart';

void main(){
  final String userName ="Eko kurniadi";
  final String email ="ekokurniadi@gmail.com";
  final String password ="12345678"; 
  test("When construct a User model"
      "should present the correct attributes", (){
        final User user = User(userName: userName, email: email, password: password);
        expect(user.userName, userName);
        expect(user.email, email);
        expect(user.password, password);
      });
  test("When construct a model from JSON "
  "Should present the correct atributes", (){
    final User user = User.fromJson(FakeUser.user);
    expect(user.userName, userName);
    expect(user.email, email);
    expect(user.password, password);
  });
}