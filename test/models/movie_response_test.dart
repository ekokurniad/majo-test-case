import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_response.dart';

import '../resource/fake_data.dart';

void main() {
  test(
      "When construct a Movie Repsonse model from JSON "
      "Should present the correct atributes", () {
    final movieResponse = MovieResponse.fromJson(Fakejson.jsonData);
    for (int i = 0; i < movieResponse.data!.length; i += 1) {
      expect(movieResponse.data![i].i!.height, 1500);
      expect(movieResponse.data![i].i!.imageUrl,
          "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_.jpg");
      expect(movieResponse.data![i].i!.width, 1102);
      expect(movieResponse.data![i].id, "tt0944947");
      expect(movieResponse.data![i].l, "Game of Thrones");
      expect(movieResponse.data![i].q, "TV series");
      expect(movieResponse.data![i].rank, 22);
      expect(movieResponse.data![i].s, "Emilia Clarke, Peter Dinklage");
      for (int j = 0; j < movieResponse.data![i].series!.length; j += 1) {
        List<String> imageUrlseries = [
          "https://m.media-amazon.com/images/M/MV5BZTg4YzdjNTctNDg5Mi00ZmU1LTkzOWEtNmMyNDBjZjNhNTJiXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_.jpg",
          "https://m.media-amazon.com/images/M/MV5BMTljMTZmNDUtNTEzNy00NDgyLTk2N2QtOTI3MGQyNWE0MTI5XkEyXkFqcGdeQWplZmZscA@@._V1_.jpg",
          "https://m.media-amazon.com/images/M/MV5BMTg0ODM4NTc3OV5BMl5BanBnXkFtZTgwODAwODE1OTE@._V1_.jpg"
        ];
        expect(movieResponse.data![i].series![j].i!.imageUrl, imageUrlseries[j]);
        List<int> heightImageUrlseries = [720, 1080, 720];
        expect(
            movieResponse.data![i].series![j].i!.height, heightImageUrlseries[j]);
        List<int> widthImageUrlseries = [1280, 1920, 1280];
        expect(movieResponse.data![i].series![j].i!.width, widthImageUrlseries[j]);
      }
    }
  });
}
