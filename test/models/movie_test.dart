import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie.dart';

void main() {
  const _jsonResponse = {
    "Title": "Game of Thrones",
    "Poster":
        "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_.jpg"
  };

  final _title = "Game of Thrones";
  final _poster =
      "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_.jpg";

  test(
      "When construct a model"
      "should present the correct attributes",
      () {
        final movieModel = Movie(title: _title,poster: _poster);
        expect(movieModel.title, _title);
        expect(movieModel.poster, _poster);
      });
  
  test("When construct a model from JSON "
  "Should present the correct atributes", (){
    final movieModel = Movie.fromJson(_jsonResponse);
    expect(movieModel.title, _title);
    expect(movieModel.poster, _poster);
  });

  
}
