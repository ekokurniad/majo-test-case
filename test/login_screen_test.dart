import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group("Login Test", () {
    test("Test Validasi email ketika user tidak mengisi email akan me-return false",() {
      var email = EmailValidator.validate('');
      expect(email, isFalse);
    });
    test("Test Validasi email ketika format yang dimasukan tidak sesuai dengan format email",() {
      var email = EmailValidator.validate('ekokurniadigmail.com');
      expect(email, isFalse);
    });

    test("Test Validasi password ketika user tidak mengisi password ",() {
      TextEditingController password  = TextEditingController();
      password.text = '';
      expect(password.text, isEmpty);
    });

    test("Test Validasi length password min 6 character", (){
      TextEditingController password  = TextEditingController();
      password.text = '123456';
      expect(password.text.length, greaterThanOrEqualTo(6));
    });

  });
}
