import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/blocs.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group("Page Bloc Cubit", () {
    blocTest("Initial Value", build: () => PageCubit(), expect: () => []);

    blocTest<PageCubit, PageBlocState>("Starting page emit[OnSplashPage]",
        build: () => PageCubit(),
        act: (PageCubit bloc) => bloc..splashPage(),
        expect: () => [OnSplashPage()]);

    blocTest("Login Page emit[OnLoginPage()]",
        build: () => PageCubit(),
        act: (PageCubit bloc) => bloc..loginPage(),
        expect: () => [OnLoginPage()]);

    blocTest("Register Page emit[OnRegisterPage()]",
        build: () => PageCubit(),
        act: (PageCubit bloc) => bloc..registerPage(),
        expect: () => [OnRegisterPage()]);

    blocTest("Home Page emit[OnMainPage()]",
        build: () => PageCubit(),
        act: (PageCubit bloc) async {
          bloc..homePage();
        },
        expect: () => []);
  });
}
