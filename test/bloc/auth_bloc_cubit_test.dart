import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/users_service.dart';
import 'package:mockito/mockito.dart';

import '../resource/fake_user.dart';

class MockUserService extends Mock implements UserService {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group("Auth Bloc Cubit", () {
    blocTest(
      "Initials AuthBlocInitialState",
      build: () => AuthBlocCubit(),
      wait: const Duration(milliseconds: 500),
      expect: () => [],
    );

    blocTest(
      "Test Auth Logout",
      build: () => AuthBlocCubit(),
      act: (AuthBlocCubit cubit) async {
        cubit.logout();
      },
      expect: () => [],
    );
    blocTest(
      "Test Auth Login",
      build: () => AuthBlocCubit(),
      act: (AuthBlocCubit cubit) async {
        final userService = MockUserService();
        final String email = "ekokurniadi@gmail.com";
        final String password = "12345678";
        when(userService.getUserByEmailAndPassword(email, password))
            .thenAnswer((_) async => User.fromJson(FakeUser.user));
        final response =
            await userService.getUserByEmailAndPassword(email, password);
        User user = User(email: response.email, password: response.password);
        cubit..prosesLogin(user);
      },
      expect: () => [AuthBlocLoadingState(true)],
    );
  });
}
