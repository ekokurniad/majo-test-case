import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/users_service.dart';
import 'package:mockito/mockito.dart';

import '../resource/fake_user.dart';

class MockUserService extends Mock implements UserService {}

void main() {
  group("User Service", () {
    final userService = MockUserService();
    final String email = "ekokurniadi@gmail.com";
    final String password = "12345678";
    test("Get User by email and password", () async {
      when(userService.getUserByEmailAndPassword(email, password))
          .thenAnswer((_) async => User.fromJson(FakeUser.user));
      var response =
          await userService.getUserByEmailAndPassword(email, password);
      expect(response.email, email);
      expect(response.password, password);
    });

    test("Check if email is already exists", () async {
      when(userService.getUserByEmail(email))
          .thenAnswer((_) async => User.fromJson(FakeUser.user));
      var response = await userService.getUserByEmail(email);
      expect(response.email, email);
    });
    
  });
}
