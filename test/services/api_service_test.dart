import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:mockito/mockito.dart';
import '../resource/fake_data.dart';

class MockApiServices extends Mock implements ApiServices {}

void main() {
  group("Fetch movie", () {
    final service = MockApiServices();
    test("Return movie list when request complete", () async {
      when(service.getMovieList())
          .thenAnswer((_) async => MovieResponse.fromJson(Fakejson.jsonData));
      var response = await service.getMovieList();
      MovieResponse result = response;
      expect(await service.getMovieList(), isA<MovieResponse>());
      expect(result.data!.length, 1);
    });
  });
}
