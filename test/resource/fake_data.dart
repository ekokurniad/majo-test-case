class Fakejson {
  static const jsonData = {
    "d": [
      {
        "i": {
          "height": 1500,
          "imageUrl":
              "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_.jpg",
          "width": 1102
        },
        "id": "tt0944947",
        "l": "Game of Thrones",
        "q": "TV series",
        "rank": 22,
        "s": "Emilia Clarke, Peter Dinklage",
        "v": [
          {
            "i": {
              "height": 720,
              "imageUrl":
                  "https://m.media-amazon.com/images/M/MV5BZTg4YzdjNTctNDg5Mi00ZmU1LTkzOWEtNmMyNDBjZjNhNTJiXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_.jpg",
              "width": 1280
            },
            "id": "vi59490329",
            "l": "Official Series Trailer",
            "s": "3:19"
          },
          {
            "i": {
              "height": 1080,
              "imageUrl":
                  "https://m.media-amazon.com/images/M/MV5BMTljMTZmNDUtNTEzNy00NDgyLTk2N2QtOTI3MGQyNWE0MTI5XkEyXkFqcGdeQWplZmZscA@@._V1_.jpg",
              "width": 1920
            },
            "id": "vi1097842713",
            "l":
                "The 8 Most Surprising Moments From \"Game of Thrones\" to Rewatch",
            "s": "3:39"
          },
          {
            "i": {
              "height": 720,
              "imageUrl":
                  "https://m.media-amazon.com/images/M/MV5BMTg0ODM4NTc3OV5BMl5BanBnXkFtZTgwODAwODE1OTE@._V1_.jpg",
              "width": 1280
            },
            "id": "vi2166011673",
            "l": "Season 7 In-Production Teaser",
            "s": "2:03"
          }
        ],
        "vt": 302,
        "y": 2011,
        "yr": "2011-2019"
      },
    ],
    "q": "game of thr",
    "v": 1
  };
}
