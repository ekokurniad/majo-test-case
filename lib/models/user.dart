class User {
String email;
String userName;
String password;

  User({this.email="",this.password="",this.userName=""});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
    'username' : userName
  };
}