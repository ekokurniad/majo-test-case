part of 'network_bloc_cubit.dart';

abstract class NetworkBlocState extends Equatable {
  const NetworkBlocState();
  @override
  List<Object> get props => [];
}

class NetworkBlocInitial extends NetworkBlocState {
  @override
  List<Object> get props => [];
}

class NetworkBlocIsConnect extends NetworkBlocState {
  @override
  List<Object> get props => [];
}

class NetworkBlocIsDisconnect extends NetworkBlocState {
  @override
  List<Object> get props => [];
}
