import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/users_service.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterInitial());

  void register(User user) async {
    emit(RegisterLoading(true));
    var userService = UserService();
    var validationUser = await userService.getUserByEmail(user.email);
    if (validationUser.length > 0) {
      emit(RegisterLoading(false));
      emit(RegisterError("Your email is already registered"));
    } else {
      var response = await userService.insertUser(user);
      if (response > 0) {
        emit(RegisterLoading(false));
        emit(RegisterSuccess("Successfully registered"));
      } else {
        emit(RegisterLoading(false));
        emit(RegisterError("Oops Something went wrong, please try again"));
      }
    }
  }

  void setInit() {
    emit(RegisterInitial());
  }
}
