import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/users_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState(false));
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState(true));
      } else {
        emit(AuthBlocLoginState(false));
      }
    }
  }

  prosesLogin(User user) async {
    emit(AuthBlocLoadingState(true));
    var userService = UserService();
    var response =
        await userService.getUserByEmailAndPassword(user.email, user.password);
    var totalData = response.length;
    if (totalData == 0) {
      emit(AuthBlocErrorState("Login gagal, periksa kembali inputan anda"));
      Future.delayed(Duration(seconds: 1), () {
        emit(AuthBlocLoadingState(false));
      });
    } else {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setBool("is_logged_in", true);
      sharedPreferences.setString("username", response[0].userName);
      emit(AuthBlocLoginState(true));
      emit(AuthBlocLoadingState(false));
      emit(AuthBlocSuccesState("Login Berhasil"));
    }
  }

  void setInit() {
    emit(AuthBlocInitialState());
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoadingState(true));
    });
    await sharedPreferences.remove("is_logged_in");
    await sharedPreferences.remove("username");
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoadingState(false));
    });
  }
}
