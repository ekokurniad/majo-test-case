import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocInitialState());
    ApiServices apiServices = ApiServices();
    MovieResponse? movieResponse = await apiServices.getMovieList();
    if (movieResponse.data != null) {
      emit(HomeBlocErrorState("Please check your connection internet"));
    } else {
      emit(HomeBlocLoadedState(movieResponse.data!));
    }
  }

  void goToDetail(data) {
    emit(HomeBlocDetailMovie(data));
  }
}
