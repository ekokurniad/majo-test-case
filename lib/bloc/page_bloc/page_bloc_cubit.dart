import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'page_bloc_state.dart';

class PageCubit extends Cubit<PageBlocState> {
  PageCubit() : super(PageBlocInitial());

  void splashPage() async {
    emit(OnSplashPage());
  }

  void checkLogin() async {
    emit(PageBlocInitial());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    String? username =  sharedPreferences.getString("username");
    if(isLoggedIn == null){
      emit(OnSplashPage());
    }else{
      emit(OnMainPage(username!));
    }
  }

  void loginPage() async {
    emit(OnLoginPage());
  }

  void registerPage() async {
    emit(OnRegisterPage());
  }

  void homePage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? username =  sharedPreferences.getString("username");
    emit(OnMainPage(username!));
  }
}
