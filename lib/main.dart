import 'package:majootestcase/bloc/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/ui/wrapper/pages.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => PageCubit()),
            BlocProvider(create: (_) => AuthBlocCubit()),
            BlocProvider(create: (_) => RegisterBlocCubit()),
            BlocProvider(create: (_) => HomeBlocCubit()),
            BlocProvider(create: (_) => NetworkBlocCubit()),
          ],
          child: Wrapper(),
        ));
  }
}
