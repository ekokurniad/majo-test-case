import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/local_service.dart';

class UserService {
  getUserByEmailAndPassword(String email, String password) async {
    return await LocalService.db.validateUser(email, password);
  }
  
  getUserByEmail(String email) async {
    return await LocalService.db.validateUserByEmail(email);
  }

  insertUser(User user) async {
    return await LocalService.db.createUser(user);
  }
}
