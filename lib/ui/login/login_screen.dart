part of '../wrapper/pages.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool isEmailValid = true;
  bool isPasswordValid = true;
  bool isSigningIn = false;
  bool _isObscurePassword = true;
  final Toast toast = Toast();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BlocProvider.of<PageCubit>(context)..splashPage();
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider<AuthBlocCubit>(
          create: (_) => AuthBlocCubit(),
          child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
            builder: (context, state) {
              if (state is AuthBlocErrorState) {
                Future.microtask(
                    () async => await toast.toastError(context, state.error));
              } else if (state is AuthBlocSuccesState) {
                Future.microtask(() async =>
                    await toast.toastSuccess(context, state.message));
                BlocProvider.of<PageCubit>(context)..homePage();
              } else if (state is AuthBlocLoadingState) {
                if (state.isLoading == true) {
                  isSigningIn = true;
                } else {
                  isSigningIn = false;
                }
              } else if (state is AuthBlocInitialState) {
                isSigningIn = false;
              }
              return Container(
                margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 70),
                        SizedBox(
                          height: 70,
                          child: Image.asset(
                            'images/main-logo.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 30, bottom: 40),
                          child: Text("Welcome Back,\nPlease Login First !",
                              style: blackTextFont.copyWith(fontSize: 20)),
                        ),
                        TextField(
                          onChanged: (text) {
                            setState(() {
                              isEmailValid = EmailValidator.validate(text);
                            });
                            BlocProvider.of<AuthBlocCubit>(context)..setInit();
                          },
                          controller: _emailController,
                          decoration: InputDecoration(
                              labelStyle: greyTextFont.copyWith(fontSize: 16),
                              hintStyle: greyTextFont.copyWith(fontSize: 16),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              labelText: "Email Address",
                              hintText: "Email Address"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Visibility(
                              child: Text(
                                "Please insert a valid email",
                                style: redTextFont.copyWith(fontSize: 13),
                              ),
                              visible: !isEmailValid),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextField(
                          onChanged: (text) {
                            setState(() {
                              isPasswordValid = text.length >= 6;
                            });
                            BlocProvider.of<AuthBlocCubit>(context)..setInit();
                          },
                          controller: _passwordController,
                          obscureText: _isObscurePassword,
                          decoration: InputDecoration(
                              labelStyle: greyTextFont.copyWith(fontSize: 16),
                              hintStyle: greyTextFont.copyWith(fontSize: 16),
                              suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _isObscurePassword = !_isObscurePassword;
                                    });
                                  },
                                  icon: Icon(
                                    _isObscurePassword
                                        ? Icons.visibility_off_outlined
                                        : Icons.visibility_outlined,
                                  )),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              labelText: "Password",
                              hintText: "Password"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Visibility(
                              child: Text(
                                "Password must be at least 6 characters",
                                style: redTextFont.copyWith(fontSize: 13),
                              ),
                              visible: !isPasswordValid),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 46,
                            child: isSigningIn
                                ? SpinKitFadingCircle(
                                    color: mainColor,
                                  )
                                : Container(
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: mainColor,
                                        ),
                                        onPressed: () async {
                                          if (_emailController.text == "" ||
                                              _passwordController.text == "") {
                                            toast.toastError(context,
                                                "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                                            setState(() {
                                              isEmailValid = false;
                                              isPasswordValid = false;
                                            });
                                          } else if (isEmailValid == false) {
                                            toast.toastError(context,
                                                "Masukkan e-mail yang valid");
                                          } else {
                                            User user = User(
                                                email: _emailController.text,
                                                password:
                                                    _passwordController.text);
                                            BlocProvider.of<AuthBlocCubit>(
                                                context)
                                              ..prosesLogin(user);
                                            if (state is AuthBlocLoadingState) {
                                              setState(() {
                                                isSigningIn = state.isLoading;
                                              });
                                            }
                                          }
                                        },
                                        child: Text(
                                          "Login",
                                          style: whiteTextFont.copyWith(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500),
                                        )),
                                  ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Not have an account? ",
                              style: GoogleFonts.raleway().copyWith(
                                  fontSize: 15,
                                  color: accentColor3,
                                  fontWeight: FontWeight.w400),
                            ),
                            GestureDetector(
                              onTap: () {
                                BlocProvider.of<PageCubit>(context)
                                  ..registerPage();
                              },
                              child: Text(
                                "Sign Up",
                                style: GoogleFonts.raleway().copyWith(
                                    fontSize: 15,
                                    color: mainColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
