part of '../wrapper/pages.dart';

class StartingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(children: [
        Container(
          margin: EdgeInsets.only(top: 54),
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 136,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/main-logo.png')),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 70, bottom: 16),
                      child: Text(
                        "Welcome",
                        style: GoogleFonts.raleway().copyWith(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 70, bottom: 16),
                      height: 40,
                      child: LottieBuilder.asset('images/movie.json'),
                    ),
                  ],
                ),
                Text(
                  "Get update a new movie every time",
                  style: greyTextFont.copyWith(fontSize: 16),
                ),
                Container(
                  width: 250,
                  height: 46,
                  margin: EdgeInsets.only(top: 70, bottom: 19),
                  child: ElevatedButton(
                    onPressed: () {
                      BlocProvider.of<PageCubit>(context)..loginPage();
                    },
                    child: Text(
                      "Get Started",
                      style: GoogleFonts.raleway().copyWith(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    style: ElevatedButton.styleFrom(primary: mainColor),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Already have an account? ",
                      style: GoogleFonts.raleway().copyWith(
                          fontSize: 15,
                          color: accentColor3,
                          fontWeight: FontWeight.w400),
                    ),
                    GestureDetector(
                      onTap: () {
                        BlocProvider.of<PageCubit>(context)..loginPage();
                      },
                      child: Text(
                        "Login",
                        style: GoogleFonts.raleway().copyWith(
                            fontSize: 15,
                            color: mainColor,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                )
              ]),
        ),
      ]),
    );
  }
}
