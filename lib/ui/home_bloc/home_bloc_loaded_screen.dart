import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/blocs.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/shared/shared.dart';
import 'package:majootestcase/ui/wrapper/pages.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key , this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NetworkBlocCubit, NetworkBlocState>(
      builder: (context, state) {
        if (state is NetworkBlocIsDisconnect) {
          BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
        }
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.20,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: defaultMargin),
                        height: MediaQuery.of(context).size.height * 0.17,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: mainColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            BlocBuilder<PageCubit, PageBlocState>(
                              builder: (context, state) {
                                if (state is OnMainPage) {
                                  return Padding(
                                    padding: const EdgeInsets.only(top:8.0),
                                    child: Text(
                                      "Welcome back\n${state.username}",
                                      style: yellowNumberFont.copyWith(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "Welcome back\nUser",
                                    style: yellowNumberFont.copyWith(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  );
                                }
                              },
                            ),
                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                image: DecorationImage(
                                  image: AssetImage("images/logo.png"),
                                  fit: BoxFit.contain,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                spreadRadius: 0,
                                blurRadius: 0,
                                offset:
                                    Offset(0, 0), // changes position of shadow
                              )
                            ],
                          ),
                          child: TextField(
                            style: TextStyle(color: Colors.blueGrey),
                            decoration: InputDecoration(
                              icon: Icon(Icons.search, color: Colors.grey),
                              border: InputBorder.none,
                              hintText: "Search a movie",
                              hintStyle: TextStyle(color: Colors.blueGrey),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 24,top:12),
                  child: Text(
                    "List Of Movies",
                    style: blackTextFont.copyWith(
                        fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(12, 24, 12, 15),
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom -
                      100,
                  child: GridView.builder(
                    semanticChildCount: 2,
                    physics: ScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 12,
                      mainAxisSpacing: 12,
                      childAspectRatio: 0.7,
                    ),
                    itemCount: data?.length ?? 0,
                    itemBuilder: (context, index) {
                      if (data!.length <= 0) {
                        return CircularProgressIndicator();
                      }
                      return GestureDetector(
                          onTap: () {
                            BlocProvider.of<HomeBlocCubit>(context)
                              ..goToDetail(data![index]);
                          },
                          child: MovieCard(
                            data: data![index],
                          ));
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
