part of '../wrapper/pages.dart';

class Toast {
   toastSuccess(BuildContext context,String message){
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: mainColor,
      title: "Success",
      message: "$message",
      duration: Duration(seconds: 3),
    )..show(context);
  }
   toastError(BuildContext context,String message){
      Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.pink,
      title: "Error",
      message: "$message",
      duration: Duration(seconds: 3),
    )..show(context);
  }
}
