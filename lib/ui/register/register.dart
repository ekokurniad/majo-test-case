part of '../wrapper/pages.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _usernameController = TextEditingController();

  bool isEmailValid = true;
  bool isPasswordValid = true;
  bool isSigningIn = false;
  bool _isObscurePassword = true;
  final Toast toast = Toast();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BlocProvider.of<PageCubit>(context)..loginPage();
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider(
          create: (_) => RegisterBlocCubit(),
          child: BlocBuilder<RegisterBlocCubit, RegisterBlocState>(
            builder: (context, state) {
              if (state is RegisterError) {
                Future.microtask(
                    () async => await toast.toastError(context, state.message));
              } else if (state is RegisterAlreadyExists) {
                Future.microtask(
                    () async => await toast.toastError(context, state.message));
              } else if (state is RegisterSuccess) {
                Future.microtask(() async =>
                    await toast.toastSuccess(context, state.message));
                BlocProvider.of<PageCubit>(context)..loginPage();
              }
              return Container(
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 70),
                        SizedBox(
                          height: 70,
                          child: Image.asset(
                            'images/main-logo.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 30, bottom: 40),
                          child: Text("Welcome Back,\nCreate an account",
                              style: blackTextFont.copyWith(fontSize: 20)),
                        ),
                        TextField(
                          controller: _usernameController,
                          decoration: InputDecoration(
                              labelStyle: greyTextFont.copyWith(fontSize: 16),
                              hintStyle: greyTextFont.copyWith(fontSize: 16),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              labelText: "Username",
                              hintText: "Username"),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextField(
                          onChanged: (text) {
                            setState(() {
                              isEmailValid = EmailValidator.validate(text);
                            });
                            BlocProvider.of<RegisterBlocCubit>(context)
                              ..setInit();
                          },
                          controller: _emailController,
                          decoration: InputDecoration(
                              labelStyle: greyTextFont.copyWith(fontSize: 16),
                              hintStyle: greyTextFont.copyWith(fontSize: 16),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              labelText: "Email Address",
                              hintText: "Email Address"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Visibility(
                              child: Text(
                                "Please insert a valid email",
                                style: redTextFont.copyWith(fontSize: 13),
                              ),
                              visible: !isEmailValid),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextField(
                          onChanged: (text) {
                            setState(() {
                              isPasswordValid = text.length >= 6;
                            });
                            BlocProvider.of<RegisterBlocCubit>(context)
                              ..setInit();
                          },
                          controller: _passwordController,
                          obscureText: _isObscurePassword,
                          decoration: InputDecoration(
                              labelStyle: greyTextFont.copyWith(fontSize: 16),
                              hintStyle: greyTextFont.copyWith(fontSize: 16),
                              suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _isObscurePassword = !_isObscurePassword;
                                    });
                                  },
                                  icon: Icon(
                                    _isObscurePassword
                                        ? Icons.visibility_off_outlined
                                        : Icons.visibility_outlined,
                                  )),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              labelText: "Password",
                              hintText: "Password"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Visibility(
                              child: Text(
                                "Password must be at least 6 characters",
                                style: redTextFont.copyWith(fontSize: 13),
                              ),
                              visible: !isPasswordValid),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 46,
                            child: (state is RegisterLoading) == true
                                ? SpinKitFadingCircle(
                                    color: mainColor,
                                  )
                                : GestureDetector(
                                    child: Container(
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              primary: mainColor),
                                          onPressed: () {
                                            if (_emailController.text.isEmpty ||
                                                _passwordController
                                                    .text.isEmpty ||
                                                _usernameController
                                                    .text.isEmpty) {
                                              Future.microtask(() async =>
                                                  await toast.toastError(
                                                      context,
                                                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"));
                                            } else {
                                              User user = User(
                                                  email: _emailController.text,
                                                  password:
                                                      _passwordController.text,
                                                  userName:
                                                      _usernameController.text);
                                               BlocProvider.of<RegisterBlocCubit>(context)..register(user);
                                            }
                                          },
                                          child: Text(
                                            "Sign Up",
                                            style: whiteTextFont.copyWith(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500),
                                          )),
                                    ),
                                  ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Already have an account? ",
                              style: GoogleFonts.raleway().copyWith(
                                  fontSize: 15,
                                  color: accentColor3,
                                  fontWeight: FontWeight.w400),
                            ),
                            GestureDetector(
                              onTap: () {
                                 BlocProvider.of<PageCubit>(context)..loginPage();
                              },
                              child: Text(
                                "Login",
                                style: GoogleFonts.raleway().copyWith(
                                    fontSize: 15,
                                    color: mainColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
