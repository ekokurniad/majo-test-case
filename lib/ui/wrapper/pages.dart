import 'dart:async';

import 'package:another_flushbar/flushbar.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:majootestcase/bloc/blocs.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_loaded_screen.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import '../../shared/shared.dart';
part '../splash_page/starting_page.dart';
part '../login/login_screen.dart';
part '../register/register.dart';
part '../widgets/toast.dart';
part '../extra/loading.dart';
part '../home/home_screen.dart';
part '../home/home.dart';
part '../home/detail_movie.dart';
part '../notification/notification_screen.dart';
part '../profile/profile_screen.dart';
part '../widgets/movie_card.dart';

part 'wrapper.dart';

