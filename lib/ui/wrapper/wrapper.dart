part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     BlocProvider.of<PageCubit>(context)..checkLogin();
    return Scaffold(
      body: BlocBuilder<PageCubit, PageBlocState>(builder: (_, state) {
        if (state is PageBlocInitial) {
          return SplashScreenPage();
        } else if (state is OnSplashPage) {
          return StartingPage();
        } else if (state is OnLoginPage) {
          return LoginScreen();
        } else if (state is OnRegisterPage) {
          return RegisterScreen();
        } else if (state is OnMainPage) {
          BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
          return Home();
        }
        return Container();
      }),
    );
  }
}

class SplashScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Center(
            child: Image.asset('images/main-logo.png'),
          ),
        ),
      ],
    ));
  }
}
