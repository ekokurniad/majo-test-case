part of '../wrapper/pages.dart';

class LoadingIndicator extends StatelessWidget {
  final double? height;
  final Color? color;

  const LoadingIndicator({Key? key, this.height = 10, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 70,
                child: Image.asset(
                  'images/main-logo.png',
                  fit: BoxFit.contain,
                ),
              ),
              Container(
                child: Text("Mohon menunggu sebentar.."),
              )
            ],
          ),
        ),
      ),
    );
  }
}
