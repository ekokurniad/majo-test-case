import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/blocs.dart';
import 'package:majootestcase/shared/shared.dart';

class ErrorScreen extends StatelessWidget {
  final String? message;
  final Function()? retry;
  final Color? textColor;
  final double? fontSize;
  final double? gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<HomeBlocCubit>(
        create: (_) => HomeBlocCubit(),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                message!,
                style:
                    TextStyle(fontSize: 12, color: textColor ?? Colors.black),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: accentColor2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.refresh_rounded),
                        Text(
                          "Refresh",
                          style: blackTextFont.copyWith(fontSize: 15),
                        )
                      ],
                    ),
                    onPressed: () {
                      BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
