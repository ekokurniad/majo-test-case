part of '../wrapper/pages.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int bottomNavBarIndex = 0;
  PageController? pageController;
  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = 0;
    pageController = PageController(initialPage: bottomNavBarIndex);
    BlocProvider.of<NetworkBlocCubit>(context)..checkNetwork(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: true,
      appBar: AppBar(
        backgroundColor: mainColor,
        automaticallyImplyLeading: false,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top:35,left:15),
                  child: Image.asset(
                    'images/welcome.png',
                  ),
                )
              ],
            ),
          ),
        ),
        elevation: 0,
        actions: <Widget>[
          BlocBuilder<NetworkBlocCubit, NetworkBlocState>(
            builder: (context, state) {
              if (state is NetworkBlocInitial) {
                return Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Icon(Icons.wifi, color: Colors.green),
                );
              } else if (state is NetworkBlocIsDisconnect) {
                return Container(
                  child: Icon(
                    Icons.network_check_outlined,
                    color: Colors.red,
                  ),
                );
              } else {
                return Container(
                  child: Icon(
                    Icons.network_check_outlined,
                    color: Colors.white,
                  ),
                );
              }
            },
          ),
          PopupMenuButton(
            onSelected: (value) {
              BlocProvider.of<AuthBlocCubit>(context)..logout();
              BlocProvider.of<PageCubit>(context)..splashPage();
            },
            offset: const Offset(0, 300),
            icon: Icon(Icons.logout, color: Color(0xFF2c406e)),
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 0,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.exit_to_app, color: Colors.black),
                    SizedBox(width: 8.0),
                    Text("Logout",
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, color: Color(0xFF70747F))),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
      body: Align(
          alignment: AlignmentDirectional.bottomCenter,
          child: Stack(
            children: [
              PageView(
                controller: pageController,
                onPageChanged: (index) {
                  setState(() {
                    bottomNavBarIndex = index;
                  });
                },
                children: [
                  HomeScreens(),
                  NotificationScreen(),
                  Profile(),
                ],
              )
            ],
          )),
      bottomNavigationBar: BottomNavyBar(
        showElevation: true,
        itemCornerRadius: 24,
        selectedIndex: bottomNavBarIndex,
        onItemSelected: (index) => setState(() {
          bottomNavBarIndex = index;
          pageController?.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.ease);
        }),
        items: [
          BottomNavyBarItem(
              icon: Icon(Icons.pages),
              title: Text('Home', style: blackTextFont.copyWith(fontSize: 15)),
              activeColor: mainColor,
              inactiveColor: Color(0xFF70747F),
              textAlign: TextAlign.start),
          BottomNavyBarItem(
              icon: Icon(Icons.notifications),
              title: Text('Info', style: blackTextFont.copyWith(fontSize: 15)),
              activeColor: mainColor,
              inactiveColor: Color(0xFF70747F),
              textAlign: TextAlign.start),
          BottomNavyBarItem(
              icon: Icon(Icons.person_outline),
              title: Text('Profile', style: blackTextFont.copyWith(fontSize: 15)),
              activeColor: mainColor,
              inactiveColor: Color(0xFF70747F),
              textAlign: TextAlign.start),
        ],
      ),
    );
  }
}
