part of '../wrapper/pages.dart';

class HomeScreens extends StatelessWidget {
  const HomeScreens({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocInitialState) {
        return Center(
          child: SpinKitFadingCircle(
            color: accentColor2,
          ),
        );
      }
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(message: state.error);
      } else if (state is HomeBlocDetailMovie) {
        return DetailMovie(state.data);
      }
      return Center(child: Text("Not Implement"));
    });
  }
}
